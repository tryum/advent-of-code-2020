import collections
import time

trace = False

game_count = 1
round_count = 0

def playCombat(p1_deck: collections.deque, p2_deck: collections.deque, game_idx: int):
    global game_count
    global round_count

    round_n = 1

    p1_deck_set = set()
    p2_dec_set = set()
    if trace:
        print("")
        print("=== Game {} ===".format(game_idx))
        print("")

    while p1_deck and p2_deck:

        if (tuple(p1_deck),tuple(p2_deck)) in p1_deck_set:
            return (1,0)

        p1_deck_set.add((tuple(p1_deck),tuple(p2_deck)))

        if trace:
            print("-- Round {} (Game {}) --".format(round_n, game_idx))
            print("Player 1's deck : ", p1_deck)
            print("Player 2's deck : ", p2_deck)
        p1_card = p1_deck.popleft()
        p2_card = p2_deck.popleft()
        if trace:
            print("Player 1 card : ", p1_card)
            print("Player 2 card : ", p2_card)

        p1_win = True

        p1_deck_size = len(p1_deck)
        p2_deck_size = len(p2_deck)

        if p1_card <= p1_deck_size and p2_card <= p2_deck_size:
            game_count+=1
            if trace:
                print("Playing a sub-game to determine the winner...")
            score = playCombat(collections.deque(list(p1_deck)[:p1_card]), collections.deque(list(p2_deck)[:p2_card]), game_count)
            if trace:
                print("...anyway, back to game {}.".format(game_idx))
            p1_win = score[0] > score[1]
        else:
            p1_win = p1_card > p2_card
        
        if p1_win:
            p1_deck.append(p1_card)
            p1_deck.append(p2_card)
        else:
            p2_deck.append(p2_card)
            p2_deck.append(p1_card)

        if trace:
            print("Player {} wins round {} of game {}!".format((2,1)[p1_win], round_n, game_idx))

        round_n +=1
        round_count+=1
        
        if trace:
            print("")
    
    p1_score = 0
    for i, v in enumerate(reversed(p1_deck)):
        p1_score += (i+1)*v

    p2_score = 0
    for i, v in enumerate(reversed(p2_deck)):
        p2_score += (i+1)*v
    
    return (p1_score, p2_score)

with open("input22.txt") as input:
    start = time.time()
    p1_input = True
    p1_cards = collections.deque()
    p2_cards = collections.deque()
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        line = line.strip()
        if len(line) == 0:
            p1_input = False
            continue
        if line[0] == 'P':
            continue
        if p1_input:
            p1_cards.append(int(line))
        else:
            p2_cards.append(int(line))

    score = playCombat(p1_cards, p2_cards, game_count)
    print(game_count, " games")
    print(round_count, " rounds")
    print("== Post-game results ==")
    print("Player 1's deck: ", p1_cards)
    print("Player 2's deck: ", p2_cards)

    print(score)
    end = time.time()
    print(end - start)