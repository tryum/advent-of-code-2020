
def createMap(size:int):
    game_map = []
    for z in range(size):
        plate = []
        for y in range(size):
            row = []
            for x in range(size):
                row.append(False)
            plate.append(row)
        game_map.append(plate)
    return game_map

def printMap(map_ : []):
    mid = len(map_)//2
    for z, plate in enumerate(map_):
        print("z={}".format(z-mid))
        for row in plate:
            for x in row:
                if x:
                    print('#',end='')
                else:
                    print('.',end='')
            print('')

def initMap(map_: [], input: []):
    z = len(map_)//2
    for y in range(len(map_[z])):
        offset_y = y - (len(map_[z])-len(input)) // 2
        if 0 <= offset_y < len(input):
            for x in range(len(map_[z][y])):
                offset_x = x - (len(map_[z][y])-len(input[offset_y]))//2
                if 0 <= offset_x < len(input[offset_y]):
                    if input[offset_y][offset_x] == '#' :
                        map_[z][y][x] = True

def countActiveCells(game: [], x_: int, y_: int, z_: int):
    count = 0
    for z in range(z_-1, z_+2):
        if not 0 <= z < len(game):
            continue
        for y in range(y_-1, y_+2):
            if not 0 <= y < len(game):
                continue
            for x in range(x_-1, x_+2):
                if not 0 <= x < len(game) or x==x_ and y==y_ and z==z_:
                    continue
                if game[z][y][x]:
                    count += 1
    return count

def countAllCells(game: []):
    count = 0
    dim = len(game)
    for z in range(dim):
        for y in range(dim):
            for x in range(dim):
                count += game[z][y][x]
    return count

def evolve(game: []):
    dim = len(game)
    result = createMap(len(game))
    for z in range(dim):
        for y in range(dim):
            for x in range(dim):
                count = countActiveCells(game, x, y, z)
                if game[z][y][x] and (count == 2 or count == 3):
                    result[z][y][x] = True
                if not game[z][y][x] and count == 3:
                    result[z][y][x] = True
    return result


with open("input17.txt") as input:
    initial_map = []
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        #print(line)
        initial_map.append(line.strip())

    game = createMap(len(initial_map)+12)
    initMap(game, initial_map)
    #printMap(game)
    for t in range(6):
        game = evolve(game)
        #print("After {} cycle :".format(t+1))
        #printMap(game)
    print(countAllCells(game))