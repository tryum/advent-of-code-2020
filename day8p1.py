with open('input8.txt') as input:
    accumulator = 0
    instruction_set = set()
    instructions = []
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        line = line.strip()
        instruction, argument = line.split()
        argument = int(argument)
        instructions.append((instruction, argument))

    pc = 0
    while True:
        if pc in instruction_set:
            break
        else:
            instruction_set.add(pc)

        instruction = instructions[pc]
        print(instruction)
        if instruction[0] == 'nop':
            pc += 1
        elif instruction[0] == 'acc':
            pc+= 1
            accumulator += instruction[1]
        elif instruction[0] == 'jmp' :
            pc += instruction[1]
    
    print(accumulator)

