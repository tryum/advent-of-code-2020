count = 0

with open("input6.txt") as input:
    group_answers = set()
    while True :
        line = input.readline()
        if len(line) == 1 :
            count += len(group_answers)
            group_answers.clear()
        if len(line) == 0 :
            count += len(group_answers)
            break
        line = line.strip()
        for c in line:
            group_answers.add(c)
    print(count)