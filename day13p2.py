import sys
from math import gcd

with open("input13_bonus.txt") as input:
    time = int(input.readline())
    lines = input.readline().split(',')

    best_delta = sys.maxsize
    best_id = 0
    bus_lines = []
    mult = 1
    for i, l in enumerate(lines):
        if l != 'x':
            id = int(l)
            bus_lines.append((id, i))
            mult *= id

    cumulated = 1
    time = 0
    for l in bus_lines:
        while (time + l[1])%l[0] != 0:
            time += cumulated
        print("line {} at : {}".format(l[0], time))
        cumulated *= l[0]
    print(time)
