
def part1(instructions : []):
    d = 90
    x = 0
    y = 0
    for i, v in instructions:
        if i == 'N':
            y-=v
        elif i == 'S':
            y+=v
        elif i == 'E':
            x+=v
        elif i == 'W':
            x-=v
        elif i == 'L':
            d-=v
            d+=360
            d%=360
        elif i == 'R':
            d+=v
            d+=360
            d%=360
        elif i == 'F':
            if d == 0:
                y-=v
            elif d == 90:
                x+=v
            elif d == 180:
                y+=v
            elif d == 270:
                x-=v
    print(x, y, abs(x)+abs(y))

with open("input12.txt") as input:
    instructions = []
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        instruction = line[0]
        line = line.lstrip(instruction)
        value = int(line)
        instructions.append((instruction, value))
    part1(instructions)