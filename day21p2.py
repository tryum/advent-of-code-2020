with open("input21.txt") as input:
    food = []
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        line = line.strip()
        ingredients, allergens = line.split('(')
        ingredients = ingredients.split()
        allergens = [i.strip() for i in allergens.rstrip(')').lstrip('contains').split(',')]
        food.append((ingredients, allergens))
    
    allergens = {}
    ingredients = set()
    for f in food:
        for i in f[0]:
            ingredients.add(i)
        for a in f[1]:
            if a in allergens:
                allergens[a] = allergens[a].intersection(set(f[0]))
            else:
                allergens[a] = set(f[0])
    
    while True:
        single = []
        other = []
        for k,v in allergens.items():
            if len(v) == 1:
                single.append(list(v)[0])
            elif len(v) > 1:
                other.append(k)

        if len(other) == 0:
            break

        for s in single:
            for o in other:
                if s in allergens[o]:
                    allergens[o].remove(s)

    ing = []
    a_keys = list(allergens.keys())
    a_keys.sort()
    for a in a_keys:
        ing.append(list(allergens[a])[0])

    print(','.join(ing))
