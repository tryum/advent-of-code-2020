ROWS = 128
COLS = 8

with open("input5.txt") as input:
    boarding_passes = []
    while(True):
        line = input.readline()
        if len(line) == 0:
            break
        boarding_passes.append(line.strip())

    print("\n".join(boarding_passes))

    highest_id = 0

    for boarding_pass in boarding_passes:
        row_min=0
        row_max=ROWS
        col_min=0
        col_max=COLS
        for i in range(0, 7):
            mid = (row_min + row_max) // 2
            if boarding_pass[i] == 'F':
                row_max = mid
            else:
                row_min = mid

        for i in range(7, 10):
            mid = (col_min + col_max ) //2
            if boarding_pass[i] == 'L':
                col_min = mid
            else:
                col_max = mid

        id = row_min * 8 + col_min

        if id > highest_id:
            highest_id = id
            
    print(highest_id)