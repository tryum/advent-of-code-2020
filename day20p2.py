def readTile(input):
    line = input.readline()
    tile = []
    if len(line) == 0:
        return None, None
    id = int(line.split()[1].strip(':'))
    while True:
        line = input.readline().strip()
        if len(line) == 0:
            return id, tile
        tile.append(line)

def getBorder(tile : [], index: int):
    if index == 0:
        return tile[0]
    elif index == 1:
        return ''.join([r[-1] for r in tile])
    elif index == 2:
        return tile[-1][::-1]
    elif index == 3:
        return ''.join([r[0] for r in tile])[::-1]

matches = {}

def borderMatch(border: [], tile: []):
    for j in range(4):
        border_b = getBorder(tile, j)
        a_b_match = False
        flip = False
        if border == border_b:
            a_b_match = True
            flip = True
        elif border == border_b[::-1]:
            a_b_match = True
        if a_b_match:
            return j, flip
    return None, None

def match(tile_a: [], tile_b: []):
    for i in range(4):
        border_a = getBorder(tile_a, i)
        j, flip = borderMatch(border_a, tile_b)
        if j is not None:
            return i, j, flip
    return None, None, None

def map_add(x, y, key, id_map, map_dict, matches):
    if len(id_map) == y:
        id_map.append([])
    assert(len(id_map) == x)
    id_map[y].append(key)
    
def vFlip(tile: []):
    return tile[::-1]

def hFlip(tile: []):
    return [r[::-1] for r in tile]

def rotate(tile: [], count: int):
    for r in range(count):
        tile = list(zip(*tile[::-1]))
        new_tile = []
        for r in tile:
            new_tile.append(''.join(r))
        tile = new_tile
    return tile

def printTile(tile: []):
    for r in tile:
        print(''.join(r))


with open("input20.txt") as input:
    tiles = {}
    while True:
        id, tile = readTile(input)
        if id == None:
            break
        tiles[id] = tile

    keys = list(tiles.keys())
    
    for i in range(len(keys)):
        for j in range(i+1, len(keys)):
            id_a = keys[i]
            id_b = keys[j]
            tile_a = tiles[id_a]
            tile_b = tiles[id_b]
            border_a, border_b, flip = match(tile_a, tile_b)
            if border_a is not None:
                if not id_a in matches:
                    matches[id_a] = []
                if not id_b in matches:
                    matches[id_b] = []
                matches[id_a].append((id_b, border_a, border_b, False))
                matches[id_b].append((id_a, border_b, border_a, flip))

    for k, v in matches.items():
        print(k, v)

    corner_id = None
    for k,v in matches.items():
        if len(v) == 2:
            corner_id = k
            break

    print(corner_id)

    top_left_corner = tiles[corner_id]
    h_flip = False
    v_flip = False
    edges = []
    for id, i, j, flip in matches[corner_id]:
        print(id, i, j, flip)
        edges.append(i)
        if flip:
            if i % 2 == 0:
                h_flip = True
                top_left_corner = hFlip(tiles[corner_id])
            else:
                v_flip = True
                top_left_corner = vFlip(tiles[corner_id])

    for i, edge in enumerate(edges):
        if v_flip:
            if edge == 0:
                edges[i] = 2
            if edge == 2:
                edges[i] = 0
        if h_flip:
            if edge == 1:
                edges[i] = 3
            if edge == 3:
                edges[i] = 1
                
    edges.sort()
    if edges == [0, 1]:
        top_left_corner = rotate(top_left_corner, 1)
    elif edges == [0, 3]:
        top_left_corner = rotate(top_left_corner, 2)
    elif edges == [2, 3]:
        top_left_corner = rotate(top_left_corner, 3)

    good_map = []
    good_map.append([top_left_corner])

    x = 0
    y = 0

    keys.remove(corner_id)

    border = getBorder(good_map[y][x], 1)

    printTile(top_left_corner)

    while keys:
        for k in keys:
            tile = tiles[k]
            j, flip = borderMatch(border, tile)
            if j is not None:
                tile = rotate(tile, 3-j)
                if flip:
                    tile = vFlip(tile)
                good_map[y].append(tile)
                keys.remove(k)
                x+=1
                border = getBorder(good_map[y][x], 1)
                break
        else:
            x=0
            border = getBorder(good_map[y][x], 2)
            for k in keys:
                tile = tiles[k]
                j, flip = borderMatch(border, tile)
                if j is not None:
                    tile = rotate(tile, 4-j)
                    if flip:
                        tile = hFlip(tile)
                    good_map.append([tile])
                    keys.remove(k)
                    y+=1
                    border = getBorder(good_map[y][x], 1)
                    break

    print("")
    print("")

    big_map = []

    for r in good_map:
        for i in range(len(r[0])):
            row = ""
            for t in r:
                row += t[i][1:-1]
                print(''.join(t[i]), end='')
                print(" ", end='')
            
            print("")
            if i > 0 and i < len(r[0])-1: 
                big_map.append(row)
        print("")

    print("")
    print("")

    for r in big_map:
        print(r)

    monster = ["                  # ",
               "#    ##    ##    ###",
               " #  #  #  #  #  #   "]
    
    monster_hash = 0
    for r in monster:
        for c in r:
            if c == '#':
                monster_hash += 1
    world_hash = 0
    for r in big_map:
        for c in r:
            if c == '#':
                world_hash += 1

    monster_count = 0



    for rot in range(4):
        print ("rot", rot)
        new_map = rotate(big_map, rot)
        for i in range(len(new_map)-len(monster)):
            for j in range(0, len(new_map[0])-len(monster[0])):
                    for t in range(len(monster)):
                        for u in range(len(monster[0])):
                            if monster[t][u] == '#' and new_map[i+t][j+u] != '#':
                                break
                        else:
                            continue
                        break
                    else:
                        monster_count += 1
                        print("monster found at", i, j)

    print(world_hash-monster_count*monster_hash)