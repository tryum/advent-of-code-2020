
def parse(line):
    lhs, rhs = line.split(":")
    password = rhs.strip()
    lhs, rhs = lhs.split()
    char = rhs
    min_occurence, max_occurence = lhs.split('-')
    return int(min_occurence), int(max_occurence), char, password


def isValid(min_occurence: int, max_occurence: int, char: str, password: str):
    occurence = password.count(char)
    return min_occurence <= occurence <= max_occurence


with open("input2.txt") as input:
    valid_count = 0
    while True:
        line = input.readline()
        if(len(line) == 0):
            break

        min_occurence, max_occurence, char, password = parse(line)
        if isValid(min_occurence, max_occurence, char, password):
            valid_count += 1
    print(valid_count)
