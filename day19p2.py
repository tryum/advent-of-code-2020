class Rule:
    id = None
    char = None
    sub_a = None
    sub_b = None

def parseRule(line: str):
    r = Rule()
    id, rule = line.split(":")
    r.id = int(id)
    rule = rule.strip()
    if rule[0] == '"':
        r.char = rule.strip('"')
    else:
        if rule.find('|') != -1:
            rule_a, rule_b = rule.split('|')
            r.sub_a = list(map(int, rule_a.split()))
            r.sub_b = list(map(int, rule_b.split()))
        else:
            r.sub_a = list(map(int, rule.split()))
    return r

def subValidate(message: str, message_idx:int, sub_rules:[], rules: {}):
    index_list = [message_idx]
    for i in sub_rules:
        next_round = []
        for j in index_list:
            inc = validate(message, j, i, rules)
            for k in inc:
                if k > 0:
                    next_round.append(k)
        index_list = next_round
    return index_list

def validate(message: str, message_idx: int,  index: int, rules: {}):
    result = []

    rule = rules[index]

    if message_idx >= len(message):
        return result

    if rule.char != None:
        if message[message_idx] == rule.char:
            result.append(message_idx+1)
    else:
        result.extend(subValidate(message, message_idx, rule.sub_a, rules))
        if rule.sub_b:
            result.extend(subValidate(message, message_idx, rule.sub_b, rules))
            
    return result


with open("input19.txt") as input:
    rules = {}
    entries = []
    max_size = 0
    read_rules = True
    valid_count = 0
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        line = line.strip()
        if len(line) == 0:
            read_rules = False
            continue
        
        if read_rules:
            rule = parseRule(line)
            rules[rule.id] = rule
        else:
            entries.append(line)
            if len(line) > max_size:
                max_size = len(line)

    rules[8] = parseRule("8: 42 | 42 8")
    rules[11] = parseRule("11: 42 31 | 42 11 31")
    for m in entries:
        result = validate(m, 0, 0, rules)
        if result and result[0] == len(m):
            valid_count += 1
    print(valid_count)