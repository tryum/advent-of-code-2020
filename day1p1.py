with open("input1.txt") as input:
    numbers = []
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        numbers.append(int(line))
    numbers.sort()
    for i in numbers:
        for j in numbers:
            if i+j == 2020:
                print(i,j)
                print(i*j)