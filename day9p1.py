def validate(numbers:[], preamble_size: int):
    for i in range(preamble_size+1, len(numbers)):
        sum = numbers[i]
        match = False
        for j in range(i-preamble_size, len(numbers)):
            for k in range(j+1, len(numbers)):
                a = numbers[j]
                b = numbers[k]
                if  a + b == sum:
                    #print("{} = {} + {}".format(sum, a, b))
                    match = True
                    break
            if match:
                break
        if not match:
            print("no sum for {}".format(sum))
            break


with open("input9.txt") as input:
    numbers = []
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        numbers.append(int(line))

    validate(numbers, 25)