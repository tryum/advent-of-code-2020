cups = [int(x) for x in "156794823"]

cups_count = len(cups)

move_n = 1
index = 0
for t in range(100):
    print("-- move {} --".format(move_n))
    print("cups: ", end="")

    cup_string = []
    for i, c in enumerate(cups):
        if i == index:
            cup_string.append("("+str(c)+")")
        else:
            cup_string.append(str(c))

    print(" ".join(cup_string))

    pick_up = []
    for i in range(1, 4):
        pick_up_index = (index + i) % cups_count
        pick_up.append(cups[pick_up_index])
    print("pick up: {}".format(", ".join([str(x) for x in pick_up])))

    current_cup = cups[index]
    dest = cups[index]
    while True:
        dest = (dest+cups_count) % (cups_count+1)
        if dest not in pick_up and dest in cups:
            break
    for p in pick_up:
        cups.remove(p)
    dest_index = cups.index(dest)
    print("destination: {}".format(cups[dest_index]), end="\n\n")

    for p in pick_up:
        dest_index += 1
        cups.insert(dest_index, p)

    
    cup_index = cups.index(current_cup)
    if cup_index != index:
        sub_cup_1 = cups[cup_index-index:None]
        sub_cup_2 = cups[0:cup_index-index]
        cups = sub_cup_1+sub_cup_2

    index = (index + 1) % cups_count
    move_n += 1

print("-- final --")
print("cups: ", end="")

cup_string = []
for i, c in enumerate(cups):
    if i == index:
        cup_string.append("("+str(c)+")")
    else:
        cup_string.append(str(c))
print(" ".join(cup_string))

one_index = cups.index(1)

result = cups[one_index+1:None]+cups[:one_index]
print("".join(str(c) for c in result))