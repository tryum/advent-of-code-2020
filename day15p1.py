with open("input15.txt") as input:
    numbers = list(map(int, input.readline().split(',')))
    print(numbers)
    number_dict = {}
    for i, n in enumerate(numbers[:-1]):
        number_dict[n] = i+1

    print(number_dict)

    last = numbers[-1]

    # appliquer un décalage !
    for i in range (len(numbers), 30000000):
        if not last in number_dict :
            number_dict[last] = i
            last = 0
        else:
            delta = i - number_dict[last]
            number_dict[last] = i
            last = delta
    print(last)