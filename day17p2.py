
def createMap(size:int):
    game_map = []
    for w in range(size):
        cube = []
        for z in range(size):
            plate = []
            for y in range(size):
                row = []
                for x in range(size):
                    row.append(False)
                plate.append(row)
            cube.append(plate)
        game_map.append(cube)
    return game_map

def printMap(map_ : []):
    mid = len(map_)//2
    for z, plate in enumerate(map_):
        print("z={}".format(z-mid))
        for row in plate:
            for x in row:
                if x:
                    print('#',end='')
                else:
                    print('.',end='')
            print('')

def initMap(map_: [], input: []):
    z = len(map_)//2
    w = len(map_)//2
    for y in range(len(map_[w][z])):
        offset_y = y - (len(map_[w][z])-len(input)) // 2
        if 0 <= offset_y < len(input):
            for x in range(len(map_[w][z][y])):
                offset_x = x - (len(map_[w][z][y])-len(input[offset_y]))//2
                if 0 <= offset_x < len(input[offset_y]):
                    if input[offset_y][offset_x] == '#' :
                        map_[w][z][y][x] = True

def countActiveCells(game: [], x_: int, y_: int, z_: int, w_: int):
    count = 0
    for w in range(w_-1, w_+2):
        if not 0<= w < len(game):
            continue
        for z in range(z_-1, z_+2):
            if not 0 <= z < len(game):
                continue
            for y in range(y_-1, y_+2):
                if not 0 <= y < len(game):
                    continue
                for x in range(x_-1, x_+2):
                    if not 0 <= x < len(game) or (x==x_ and y==y_ and z==z_ and w==w_):
                        continue
                    if game[w][z][y][x]:
                        count += 1
    return count

def countAllCells(game: []):
    count = 0
    dim = len(game)
    for w in range(dim):
        for z in range(dim):
            for y in range(dim):
                for x in range(dim):
                    count += game[w][z][y][x]
    return count

def evolve(game: []):
    dim = len(game)
    result = createMap(len(game))
    for w in range(dim):
        for z in range(dim):
            for y in range(dim):
                for x in range(dim):
                    count = countActiveCells(game, x, y, z, w)
                    if game[w][z][y][x] and (count == 2 or count == 3):
                        result[w][z][y][x] = True
                    if not game[w][z][y][x] and count == 3:
                        result[w][z][y][x] = True
    return result


with open("input17.txt") as input:
    initial_map = []
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        #print(line)
        initial_map.append(line.strip())

    game = createMap(len(initial_map)+12)
    initMap(game, initial_map)
    #printMap(game)
    for t in range(6):
        game = evolve(game)
        #print("After {} cycle :".format(t+1))
        #printMap(game)
    print(countAllCells(game))