
ROWS = 128
COLS = 8

with open("input5.txt") as input:
    boarding_passes = []
    boarding_pass_ids = []
    while(True):
        line = input.readline()
        if len(line) == 0:
            break
        boarding_passes.append(line.strip())

    for boarding_pass in boarding_passes:
        row_min=0
        row_max=ROWS
        col_min=0
        col_max=COLS
        for i in range(0, 7):
            mid = (row_min + row_max) // 2
            if boarding_pass[i] == 'F':
                row_max = mid
            else:
                row_min = mid

        for i in range(7, 10):
            mid = (col_min + col_max ) //2
            if boarding_pass[i] == 'L':
                col_max = mid
            else:
                col_min = mid

        id = row_min * 8 + col_min

        boarding_pass_ids.append(id)
    
    boarding_pass_ids.sort()

    last_id = 0

    empty_seats = []

    for id in boarding_pass_ids:
        if id - last_id > 1:
            for i in range(last_id+1, id):
                empty_seats.append(i)
        last_id = id
    print(empty_seats)