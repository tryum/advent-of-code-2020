def readTile(input):
    line = input.readline()
    tile = []
    if len(line) == 0:
        return None, None
    id = int(line.split()[1].strip(':'))
    while True:
        line = input.readline().strip()
        if len(line) == 0:
            return id, tile
        tile.append(line)

def getBorder(tile : [], index: int):
    if index == 0:
        return tile[0]
    elif index == 1:
        return ''.join([r[-1] for r in tile])
    elif index == 2:
        return tile[-1][::-1]
    elif index == 3:
        return ''.join([r[0] for r in tile])[::-1]

def match(tile_a: [], tile_b: []):
    for i in range(4):
        for j in range(4):
            border_a = getBorder(tile_a, i)
            border_b = getBorder(tile_b, j)
            if border_a == border_b or border_a[::-1] == border_b or border_a == border_b[::-1]:
                print("***")
                return True

    return False


with open("input20.txt") as input:
    tiles = {}
    matches = {}
    while True:
        id, tile = readTile(input)
        if id == None:
            break
        tiles[id] = tile

    keys = list(tiles.keys())
    
    for i in range(len(keys)):
        for j in range(i+1, len(keys)):
            id_a = keys[i]
            id_b = keys[j]
            tile_a = tiles[id_a]
            tile_b = tiles[id_b]
            print("matching {} and {}".format(id_a, id_b))
            if match(tile_a, tile_b):
                if not id_a in matches:
                    matches[id_a] = []
                matches[id_a].append(id_b)
                if not id_b in matches:
                    matches[id_b] = []
                matches[id_b].append(id_a)

    

    result = 1
    for k,v in matches.items():
        if len(v) == 2:
            result *=k
    print(result)


