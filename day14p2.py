with open("input14.txt") as input:
    one_mask = 0
    zero_mask = 2**36-1
    shuffle_masks = set() 
    memory = {}
    while True:
        
        line = input.readline()    
        if len(line) == 0:
            break
        if line.startswith("mask = ") :
            one_mask = 0
            zero_mask = 2**36-1
            shuffle_masks.clear()
            shuffle_masks.add(0)
            mask = line.lstrip("masks = ").strip()
            
            bit = 1
            for i, b in enumerate(mask.strip()[::-1]):
                if b == '1':
                    one_mask ^= bit
                elif b == 'X':
                    new_mask = []
                    for m in shuffle_masks:
                        new_mask.append(m^bit)
                    for m in new_mask:
                        shuffle_masks.add(m)
                    zero_mask ^= bit
                bit <<= 1

        else :
            line = line.lstrip("mem[")
            address, value = [int(n) for n in line.split("] = ")]

            address |= one_mask
            address &= zero_mask

            for m in shuffle_masks:
                memory[address|m]  = value
    print(sum(memory.values()))