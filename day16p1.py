def parseRanges(line: str):
    result_ranges = []
    name, ranges = line.split(':')
    for r in ranges.split('or'):
        r = tuple(map(int, r.split('-')))
        result_ranges.append(r)
    return name, result_ranges


with open("input16.txt") as input:
    state = 0 # 0 read range / 1 read your ticket / 2 read other tickets
    ranges = []
    result = 0
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        line = line.strip()
        
        if len(line) == 0:
            state += 1
            continue

        if state == 0:
            name, r = parseRanges(line)
            ranges.extend(r)

        if state == 1:
            if line[0] == 'y':
                continue
            continue

        if state == 2:
            if line[0] == 'n':
                continue
            
            fields = map(int, line.split(','))
            in_range = False
            for f in fields:
                for r in ranges:
                    if r[0] <= f <= r[1]:
                        in_range = True
                        break
                else :
                    result += f
                    print(f)
    print(result)
                        

