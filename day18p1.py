def parse(input: str):
    lhs = None
    rhs = None
    op = None
    p_count = 0
    p_start = 0
    for i, c in enumerate(input):

        if c == ' ':
            continue

        elif c == '(':
            if p_count == 0:
                p_start = i
            p_count += 1
            continue
        elif c == ')':
            p_count -= 1
            if p_count == 0:
                sub_expr = parse(input[p_start+1: i])
                if lhs == None:
                    lhs = sub_expr
                else:
                    rhs = sub_expr
        elif p_count == 0:
            if lhs == None:
                lhs = int(c)
            elif op == None:
                op = c
            else:
                rhs = int(c)

        if lhs != None and op != None and rhs != None:
            if op == '+':
                lhs += rhs
            elif op == '*':
                lhs *= rhs
            op = None
            rhs = None

    print("{} = {}".format(lhs, input))
    return lhs



with open("input18.txt") as input:
    result = 0
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        result += parse(line.strip())
        print('')
    print(result)
