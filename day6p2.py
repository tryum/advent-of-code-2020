count = 0

with open("input6.txt") as input:
    group_answers = set()
    first = True
    while True :
        line = input.readline()
        if len(line) == 1 :
            count += len(group_answers)
            group_answers.clear()
            first = True
            continue
        if len(line) == 0 :
            count += len(group_answers)
            break
        line = line.strip()
        if first:
            group_answers = set(line)
            first = False
        else :
            group_answers = group_answers.intersection(line)
    print(count)