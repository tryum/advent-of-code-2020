import time
start = time.time()

with open("input.txt") as input:
    while True:
        line = input.readline()
        if len(line) == 0:
            break

end = time.time()
print(end - start)