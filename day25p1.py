import time
start = time.time()

card_public_key = 17115212
door_public_key = 3667832

subject_number = 7
modulo = 20201227

card_loop = None
door_loop = None

value = 1
for i in range(1, 1000000000):
    value *= subject_number
    value %= modulo
    if value == card_public_key:
        card_loop = i
    if value == door_public_key:
        door_loop = i
    if card_loop and door_loop:
        break

encryption_key = 1
for i in range(0, door_loop):
    encryption_key *= card_public_key
    encryption_key %= modulo

print(encryption_key)


end = time.time()
print(end - start)