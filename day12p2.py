def rotate(angle:int, x :int, y:int):
    if angle == 90:
        tmp = y
        y = x
        x = -tmp
    elif angle == 180:
        x = -x
        y = -y
    elif angle == 270:
        tmp = y
        y = -x
        x = tmp
    else:
        raise ValueError

    return x, y

def part2(instructions : []):
    x = 0
    y = 0
    w_x = 10
    w_y = -1
    index = 0
    for i, v in instructions:
        if i == 'N':
            w_y -= v
        elif i == 'S':
            w_y += v
        elif i == 'E':
            w_x += v
        elif i == 'W':
            w_x -= v
        elif i == 'L':
            w_x, w_y = rotate(360-v, w_x, w_y)
        elif i == 'R':
            w_x, w_y = rotate(v, w_x, w_y)
        elif i == 'F':
            x += w_x * v
            y += w_y * v
        print("{}   {}{} : {},{}  {},{}".format(index, i,v,x, y, w_x, w_y))
        index += 1
    print(x, y, abs(x)+abs(y))

with open("input12.txt") as input:
    instructions = []
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        instruction = line[0]
        line = line.lstrip(instruction)
        value = int(line)
        instructions.append((instruction, value))
    part2(instructions)