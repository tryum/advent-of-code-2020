
VALID_PASSPORT = [
"byr",
"iyr",
"eyr",
"hgt",
"hcl",
"ecl",
"pid"
]


def isPassportValid(passport: []):
    intersection = set(VALID_PASSPORT).intersection(passport)
    return len(intersection) == len(VALID_PASSPORT)


with open("input4.txt") as input:
    valid_passports = 0
    passport=[]
    while True:
        line = input.readline()
        if len(line) < 2:
            if isPassportValid(passport):
                valid_passports+=1
            passport.clear()
            if len(line) == 0:
                break
        else:
            elements = line.split()
            for e in elements:
                key, value = e.split(':')
                passport.append(key)
    print("Valid passports : {}".format(valid_passports))