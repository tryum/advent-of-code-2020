import itertools

class Rule:
    id = None
    char = None
    sub_a = None
    sub_b = None

def parseRule(line: str):
    r = Rule()
    id, rule = line.split(":")
    r.id = int(id)
    rule = rule.strip()
    if rule[0] == '"':
        r.char = rule.strip('"')
    else:
        if rule.find('|') != -1:
            rule_a, rule_b = rule.split('|')
            r.sub_a = list(map(int, rule_a.split()))
            r.sub_b = list(map(int, rule_b.split()))
        else:
            r.sub_a = list(map(int, rule.split()))
    return r

cache = {}

def getSubChains(index: int, rules: {}):
    result = []
    rule = rules[index]

    if index in cache:
        return cache[index]

    if rule.char != None:
        result.append(rule.char)
    else:
        a_sub_chains = []
        b_sub_chains = []
        for i in rule.sub_a:
            a_sub_chains.append(getSubChains(i, rules))
        if rule.sub_b:
            for i in rule.sub_b:
                b_sub_chains.append(getSubChains(i, rules))

        a_sub_chains = itertools.product(*a_sub_chains)
        for sub in a_sub_chains:
            result.append(''.join(sub))
        if b_sub_chains:
            b_sub_chains = itertools.product(*b_sub_chains)
            for sub in b_sub_chains:
                result.append(''.join(sub))

    cache[index] = result

    return result


with open("input19.txt") as input:
    rules = {}
    read_rules = True
    valid_count = 0
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        line = line.strip()
        if len(line) == 0:
            all_matches = getSubChains(0, rules)
            read_rules = False
        
        if read_rules:
            rule = parseRule(line)
            rules[rule.id] = rule
        else:
            if line in all_matches:
                valid_count += 1
    print(valid_count)