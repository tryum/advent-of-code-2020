import sys

with open("input13.txt") as input:
    time = int(input.readline())
    lines = input.readline().split(',')

    best_delta = sys.maxsize
    best_id = 0
    for l in lines:
        if l != 'x':
            id = int(l)
            delta = id - time%id
            if delta < best_delta:
                best_delta = delta
                best_id =  id
    print(best_id, best_id*best_delta)