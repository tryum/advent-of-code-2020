import time

start = time.time()

input = "156794823"

cups = [None]*1000001

class Node:
    def __init__(self, label: int):
        self.next = None
        self.label = label

last = None
for c in input:
    label = int(c)
    cups[label] = Node(label)
    if last:
        last.next = cups[label]
    last = cups[label]

for i in range(10, 1000001):
    cups[i] = Node(i)
    last.next = cups[i]
    last = cups[i]

cups[0] = cups[int(input[0])]
cups[-1].next = cups[0]

cups_count = len(cups)

trace = False

move_n = 1
for t in range(10000000):
    pick_up = cups[0].next
    cups[0].next = cups[0].next.next.next.next
    destination_cup_label = cups[0].label-1
    while True:
        if destination_cup_label == 0:
            destination_cup_label = 1000000
        if destination_cup_label != pick_up.label and destination_cup_label != pick_up.next.label and destination_cup_label != pick_up.next.next.label:
            break
        destination_cup_label -= 1
    pick_up.next.next.next = cups[destination_cup_label].next
    cups[destination_cup_label].next = pick_up
    cups[0] = cups[0].next
    
    

print(cups[1].next.label*cups[1].next.next.label)

end = time.time()
print(end - start)