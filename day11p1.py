
def count_taken_seats(x: int, y: int, seat_map:[]):
    count = 0
    for i in range(x-1, x+2):
        for j in range(y-1, y+2):
            if i>=0 and i < len(seat_map[0]) and j>=0 and j <len(seat_map) and (i!=x or j!=y) and seat_map[j][i]=='#':
                count += 1
    return count

def simulate_turn(seat_map : []):
    new_seat_map = []
    seat_change = False
    for i in range(len(seat_map)):
        row = ""
        for j in range(len(seat_map[0])):
            count = count_taken_seats(j, i, seat_map)
            if seat_map[i][j] == 'L' and count == 0:
                seat_change = True
                row += "#"
            elif seat_map[i][j] == '#' and count >= 4:
                seat_change = True
                row += "L"
            else:
                row += seat_map[i][j]
        new_seat_map.append(row)
    return new_seat_map, seat_change

with open('input11.txt') as input:
    seat_map = []
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        seat_map.append(line.strip())
    print('\n'.join(seat_map))

    while True:
        print('\n')
        seat_map, change = simulate_turn(seat_map)
        print('\n'.join(seat_map))
        if not change:
            break
    count = 0
    for r in seat_map:
        for c in r:
            if c == '#':
                count +=1
    print(count)