import time
start = time.time()

with open("input24.txt") as input:

    black_tiles = []
    while True:
        line = input.readline()
        if len(line) == 0:
            break

        x = 0
        y = 0

        skip_next = False
        for i in range(0, len(line)):
            if skip_next:
                skip_next = False
                continue
            if line[i] == 'n':
                skip_next = True
                y+=1
                if line[i+1] == 'w':
                    x-=1
            elif line [i] == 's':
                skip_next = True
                y-=1
                if line[i+1] == 'e':
                    x+=1
            elif line [i] == 'e':
                x+=1
            elif line [i] == 'w':
                x-=1

        if (x,y) in black_tiles:
            black_tiles.remove((x,y))
        else:
            black_tiles.append((x,y))

    #print(len(black_tiles))

    for i in range(100):
        adjacent_map = {}
        next_black_tiles = []
        for b in black_tiles:
            for d in [(-1,+1),(0,+1),(+1,0),(+1,-1),(0,-1),(-1,0)]:
                x = b[0] + d[0]
                y = b[1] + d[1]
                if (x,y) in adjacent_map:
                    adjacent_map[(x,y)] += 1
                else:
                    adjacent_map[(x,y)] = 1
        for k,v in adjacent_map.items():
            if k in black_tiles:
                if 0<v<=2:
                    next_black_tiles.append(k)
            elif v == 2:
                next_black_tiles.append(k)

        black_tiles = next_black_tiles
        # print("Day {}: {}".format(i+1, len(black_tiles)))



end = time.time()
print(end - start)