import queue

with open("input22.txt") as input:
    p1_input = True
    p1_cards = queue.Queue()
    p2_cards = queue.Queue()
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        line = line.strip()
        if len(line) == 0:
            p1_input = False
            continue
        if line[0] == 'P':
            continue
        if p1_input:
            p1_cards.put(int(line))
        else:
            p2_cards.put(int(line))

    round_n = 1
    while not p1_cards.empty() and not p2_cards.empty():
        print("-- Round {} --".format(round_n))
        print("Player 1's deck : ", list(p1_cards.queue))
        print("Player 2's deck : ", list(p2_cards.queue))
        p1_card = p1_cards.get()
        p2_card = p2_cards.get()
        print("Player 1 card : ", p1_card)
        print("Player 2 card : ", p2_card)
        if p1_card > p2_card:
            print("Player 1 wins the round!")
            p1_cards.put(p1_card)
            p1_cards.put(p2_card)
        else:
            print("Player 2 wins the round!")
            p2_cards.put(p2_card)
            p2_cards.put(p1_card)
        round_n +=1
        print("")
    
    if p1_cards.empty():
        winning_deck = p2_cards
    else:
        winning_deck = p1_cards
    
    result = 0
    for i, v in enumerate(reversed(list(winning_deck.queue))):
        result += (i+1)*v
    print(result)
