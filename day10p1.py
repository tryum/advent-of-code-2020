with open("input10.txt") as input:
    adapters = []
    adapters.append(0)
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        adapters.append(int(line))
    adapters.sort()
    device = max(adapters) + 3
    adapters.append(device)

    weight = {}
    weight[0] = 1
    for i in adapters:
        for j in range(i+1, i+4):
            if j in adapters:
                if j in weight:
                    weight[j] += weight[i]
                else:
                    weight[j] = weight[i]

    print(weight[device])


