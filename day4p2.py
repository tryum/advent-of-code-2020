import re

VALID_PASSPORT = [
"byr",
"iyr",
"eyr",
"hgt",
"hcl",
"ecl",
"pid"
]

def validYear(year: str, min : int, max: int):
    return len(year) == 4 and year.isdigit() and min <= int(year) <= max

def validHeight(height: str):
    if height.rfind('cm') > 0:
        cm_height = int(height.rstrip('cm'))
        return 150 <= cm_height <= 193
    elif height.rfind('in') > 0:
        in_height = int(height.rstrip('in'))
        return 59 <= in_height <= 76
    else:
        return False

def validHairColor(hair_color: str):
    _rgbstring = re.compile(r'#[a-fA-F0-9]{6}$')
    return bool(_rgbstring.match(hair_color))

def isValidEyeColor(eye_color: str):
    _validEyeColors = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
    return eye_color in _validEyeColors

def isValidPassportNumber(passport_number: str):
    return len(passport_number) == 9 and passport_number.isdigit()

def isPassportValid(passport: {}):
    intersection = set(VALID_PASSPORT).intersection(passport)
    if len(intersection) == len(VALID_PASSPORT):
        return validYear(passport["byr"], 1920, 2002) \
            and validYear(passport["iyr"], 2010, 2020) \
            and validYear(passport["eyr"], 2020, 2030) \
            and validHeight(passport["hgt"]) \
            and validHairColor(passport["hcl"]) \
            and isValidEyeColor(passport["ecl"]) \
            and isValidPassportNumber(passport["pid"])

    else:
        return False

with open("input4.txt") as input:
    valid_passports = 0
    passport={}
    while True:
        line = input.readline()
        if len(line) < 2:
            if isPassportValid(passport):
                valid_passports+=1
            passport.clear()
            if len(line) == 0:
                break
        else:
            elements = line.split()
            for e in elements:
                key, value = e.split(':')
                passport[key] = value
    print("Valid passports : {}".format(valid_passports))
