

def parseLine(line: str):
    bags = []
    bag, sub_bags_str = line.split("contain")
    bag = bag.rstrip(' bags')
    if sub_bags_str.strip() != "no other bags." :
        sub_bags = sub_bags_str.split(',')
        for sub_bag in sub_bags:
            sub_bag = sub_bag.strip()
            sub_bag = sub_bag.rstrip('.')
            sub_bag = sub_bag.rstrip(' bags')
            sub_bag_count, sub_bag_color = sub_bag.split(maxsplit=1)
            for i in range(0, int(sub_bag_count)):
                bags.append(sub_bag_color)
    return bag, bags



with open('input7.txt') as input:
    bag_tree = {}
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        bag, bags = parseLine(line)
        if bag not in bag_tree:
            bag_tree[bag] = []
        print(bag, bags)
        for sub_bag in bags:
            if sub_bag not in bag_tree:
                bag_tree[sub_bag] = []
            bag_tree[sub_bag].append(bag)
    container_bags = set()

    Q = ['shiny gold']
    while Q:
        color = Q.pop(0)
        colors = bag_tree[color]
        for c in colors:
            if not c in container_bags:
                container_bags.add(c)
                Q.append(c)


    print(len(container_bags), container_bags)
