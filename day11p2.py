
def count_taken_seats(x: int, y: int, seat_map:[]):
    count = 0
    dir = [[-1, 0], [-1, -1], [0, -1], [1, -1],
           [1, 0], [1, 1], [0, 1], [-1, 1]]

    for d in dir:
        dx = x
        dy = y
        while True:
            dx += d[0]
            dy += d[1]
            if not(dx>=0 and dx < len(seat_map[0]) and dy>=0 and dy <len(seat_map)) or seat_map[dy][dx] == 'L':
                break
            elif seat_map[dy][dx] == '#':
                count += 1
                break

    return count

def simulate_turn(seat_map : []):
    new_seat_map = []
    seat_change = False
    for i in range(len(seat_map)):
        row = ""
        count_map = []
        for j in range(len(seat_map[0])):
            count = count_taken_seats(j, i, seat_map)
            count_map.append(count)
            if seat_map[i][j] == 'L' and count == 0:
                seat_change = True
                row += "#"
            elif seat_map[i][j] == '#' and count >= 5:
                seat_change = True
                row += "L"
            else:
                row += seat_map[i][j]
        print(count_map)
        new_seat_map.append(row)
    return new_seat_map, seat_change

with open('input11.txt') as input:
    seat_map = []
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        seat_map.append(line.strip())
    print('\n'.join(seat_map))

    while True:
        print('\n')
        seat_map, change = simulate_turn(seat_map)
        print('\n'.join(seat_map))
        if not change:
            break
    count = 0
    for r in seat_map:
        for c in r:
            if c == '#':
                count +=1
    print(count)
