def validate(numbers:[], preamble_size: int):
    for i in range(preamble_size+1, len(numbers)):
        sum = numbers[i]
        match = False
        for j in range(i-preamble_size, len(numbers)):
            for k in range(j+1, len(numbers)):
                a = numbers[j]
                b = numbers[k]
                if  a + b == sum:
                    #print("{} = {} + {}".format(sum, a, b))
                    match = True
                    break
            if match:
                break
        if not match:
            print("no sum for {}".format(sum))
            return sum, i
            break



with open("input9.txt") as input:
    numbers = []
    accumulate = []
    last = 0
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        numbers.append(int(line))
        last += int(line)
        accumulate.append(last)

    sum, index = validate(numbers, 25)

    print("{} as no preamble".format(sum))

    for i in range(0, index):
        for j in range(i+1, index):
            if accumulate[j]-accumulate[i] == sum:
                verify = 0
                for k in range(i+1, j+1):
                    print(numbers[k])
                    verify += numbers[k]
                if verify == sum:
                    print("Found : {}, {}".format(i, j))
                    result = min(numbers[i+1:j]) + max(numbers[i+1:j])
                    print("{} = {} + {}".format(result, numbers[i+1], numbers[j]))
                    break