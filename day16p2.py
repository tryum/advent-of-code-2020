def parseRanges(line: str):
    result_ranges = []
    name, ranges = line.split(':')
    for r in ranges.split('or'):
        r = tuple(map(int, r.split('-')))
        result_ranges.append(r)
    return name, result_ranges


with open("input16.txt") as input:
    state = 0 # 0 read range / 1 read your ticket / 2 read other tickets
    ranges = []
    named_ranges = {}
    result = 0
    valid_tickets = []
    my_ticket = []
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        line = line.strip()
        
        if len(line) == 0:
            state += 1
            continue

        if state == 0:
            name, r = parseRanges(line)
            ranges.extend(r)
            named_ranges[name] = r

        if state == 1:
            if line[0] == 'y':
                continue
            my_ticket = list(map(int, line.split(',')))
            continue

        if state == 2:
            if line[0] == 'n':
                continue
            
            fields = list(map(int, line.split(',')))
            assert(len(fields) == len(named_ranges))
            in_range = False
            valid_ticket = True
            for f in fields:
                for r in ranges:
                    if r[0] <= f <= r[1]:
                        in_range = True
                        break
                else :
                    valid_ticket = False
                    result += f
            if valid_ticket:
                valid_tickets.append(fields)

    print(result)
    field_mapping = {}
    for k, v in named_ranges.items():
        field_mapping[k] = []

    for k, v in named_ranges.items():
        ranges = v
        for j in range(len(named_ranges)):
            valid = True
            for t in valid_tickets:
                value = t[j]
                if (value >= ranges[0][0] and value <= ranges[0][1]) or (value >= ranges[1][0] and value <= ranges[1][1]):
                    continue
                else:
                    valid = False
                    break
            
            if valid:
                field_mapping[k].append(j)
                print("{} may be field n {}".format(k, j))
            else:
                print("{} not field n {}".format(k, j))

    print(field_mapping)

    while sum(map(len,field_mapping.values())) != len(field_mapping):
        remove_list = []
        for k, v in field_mapping.items():
            if len(v) == 1:
                remove_list.append((k, v[0]))
        for e in remove_list:
            for k, v in field_mapping.items():
                if k != e[0] and e[1] in v:
                    v.remove(e[1])

    for k, v in field_mapping.items():
        print("{} : {}".format(k, my_ticket[v[0]]))
