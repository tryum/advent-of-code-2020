with open("input14.txt") as input:
    one_mask = 0
    zero_mask = 68719476735
    memory = {}
    while True:
        
        line = input.readline()    
        if len(line) == 0:
            break
        if line.startswith("mask = ") :
            one_mask = 0
            zero_mask = 68719476735
            mask = line.lstrip("masks = ").strip()
            
            bit = 1
            for i, b in enumerate(mask.strip()[::-1]):
                if b == '1':
                    one_mask ^= bit
                elif b == '0':
                    zero_mask ^= bit
                    
                bit <<= 1
            print("     mask : {}".format(mask))
            print("one  mask : {0:36b}".format(one_mask))
            print("zero mask : {0:36b}".format(zero_mask))
        else :
            line = line.lstrip("mem[")
            address, value = [int(n) for n in line.split("] = ")]
            print(address, value)

            original = value
            #print("{0:b}".format(one_mask))
            #print("{0:b}".format(zero_mask))
            #print("{0:b}".format(value))
            value |= one_mask
            value &= zero_mask
            memory[address]  = value
    print(sum(memory.values()))