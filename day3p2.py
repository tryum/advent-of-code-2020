with open('input3.txt') as input:
    map = []
    while True :
        line = input.readline()
        if len(line) == 0 :
            break
        map.append(line.strip())
    slopes = [[1, 1], [3, 1], [5, 1], [7, 1], [1, 2]]
    result = 1
    for slope in slopes:
        tree_count = 0
        x = 0
        y = 0
        while y < len(map) :
            if map[y][x] == '#':
                tree_count += 1
            x += slope[0]
            y += slope[1]
            x %= len(map[0])
        print(x, y, tree_count)
        result *= tree_count
    print(result)