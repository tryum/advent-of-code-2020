with open('input3.txt') as input:
    map = []
    while True :
        line = input.readline()
        if len(line) == 0 :
            break
        map.append(line.strip())
    tree_count = 0
    x = 0
    y = 0
    while y < len(map) :
        if map[y][x] == '#':
            tree_count += 1
        x += 3
        y += 1
        x %= len(map[0])
    print(x, y, tree_count)