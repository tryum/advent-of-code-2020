def runProgram(instructions : []):
    accumulator = 0
    instruction_set = set()
    pc = 0
    clean_exit = True
    while pc < len(instructions):
        if pc in instruction_set:
            clean_exit = False
            break
        else:
            instruction_set.add(pc)

        instruction = instructions[pc]
        if instruction[0] == 'nop':
            pc += 1
        elif instruction[0] == 'acc':
            pc+= 1
            accumulator += instruction[1]
        elif instruction[0] == 'jmp' :
            pc += instruction[1]
    return clean_exit, accumulator

with open('input8.txt') as input:
    instructions = []
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        line = line.strip()
        instruction, argument = line.split()
        argument = int(argument)
        instructions.append((instruction, argument))

    for i in range(0, len(instructions)):
        instruction_copy = instructions.copy()
        if instruction_copy[i][0] == 'jmp':
            instruction_copy[i] = ('nop', instruction_copy[i][1])
        elif instruction_copy[i][0] == 'nop':
            instruction_copy[i] = ('jmp', instruction_copy[i][1])
        else:
            continue

        clean_exit, accumulator = runProgram(instruction_copy)

        if clean_exit:
            print(accumulator)
            break

